#! /usr/bin/env python

"""
Install inkscape and update the diagrams with:
waf --diag=1 ; waf --diag=2 ; waf --diag=3

For parallel execution diagram, the commands are:

rm -rf /tmp/build
cd $WAFDIR/utils
./genbench.py /tmp/build 20 7 5 2
cd /tmp/build
perl -pi -e "s#opt.load\('compiler_cxx'\)#opt.load('compiler_cxx parallel_debug')#g" wscript
perl -pi -e "s#stlib#shlib#g" wscript
echo "from waflib import Runner;Runner.GAP=2" >> wscript
waf configure clean build -p -j4 --dwidth=800 --dtitle='Parallel build of 20 shared libraries (waf -j4)' --dnotooltip
inkscape pdebug.svg --export-eps=weak_parallel.eps

rm -rf /tmp/build
cd $WAFDIR/utils
./genbench.py /tmp/build 50 15 5 2
cd /tmp/build
perl -pi -e "s#opt.load\('compiler_cxx'\)#opt.load('compiler_cxx parallel_debug')#g" wscript
perl -pi -e "s#stlib#shlib#g" wscript
echo "from waflib import Runner;Runner.GAP=3" >> wscript
waf configure clean build -p -j6 --dwidth=800 --dtitle='Parallel build representation - 50 shared libraries (waf -j6)' --dnotooltip
inkscape pdebug.svg --export-eps=pdebug.eps

The file dev.eps is generated from this last build using:

#! /usr/bin/env gnuplot
set terminal eps enhanced color size 6,4 font 'Helvetica,18' linewidth 1.5
set output "dev.eps"
set ylabel "Amount of active threads"
set xlabel "Amount of active threads over time (waf -j6)"
unset label
set yrange [-0.1:6.2]
set ytic 1
plot 'pdebug.dat' using 3:7 with lines title "" lt 2
"""

from waflib import Task

def options(ctx):
	ctx.load('parallel_debug')
	ctx.add_option('--diag', default='1', help='diagram 1,2 or 3')

def configure(ctx):
	ctx.load('parallel_debug')

def build(bld):
	bld.jobs = 2
	bld.options.dnotooltip = True
	diag = bld.options.diag
	if diag == '1':
		bld.options.dtitle = 'Same priority for green and red tasks (waf -j2)'
	elif diag == '2':
		bld.options.dtitle = 'Increased red task weight (waf -j2)'
	elif diag == '3':
		bld.options.dtitle = 'Gap of green tasks between red and blue (waf -j2)'
		from waflib import Runner
		Runner.GAP = 5

	if diag in '12':
		bld.options.dmaxtime=7.1
		for x in range(5):
			bld(rule='sleep 1', color='GREEN', name='green', always=True)
		tg = bld(rule='sleep 5', color='RED', name='red', always=True)
	else:
		bld(rule='sleep 1', color='BLUE', name='blue', always=True)
		for x in range(80):
			bld(rule='sleep 0.1', color='GREEN', name='green', always=True)
		tg = bld(rule='sleep 6', color='RED', name='red', always=True, after='blue')

	if diag in '23':
		tg.post()
		tg.tasks[0].weight = 20

	def copy_to(ctx):
		if diag == '1':
			name = 'tasks_nosort.eps'
		elif diag == '2':
			name = 'tasks_sort.eps'
		elif diag == '3':
			name = 'weak.eps'
		ctx.exec_command('inkscape pdebug.svg --export-eps=../../%s' % name, cwd=bld.srcnode.abspath())

	bld.add_post_fun(copy_to)

